﻿using System;

namespace State
{
    internal class HasQuarterState : IState
    {
        private readonly GumballMachine gumballMachine;

        public HasQuarterState(GumballMachine gumballMachine)
        {
            this.gumballMachine = gumballMachine;
        }

        public void Insert()
        {
            Console.WriteLine(MachineMessages.InsertTwice);
        }

        public void Eject()
        {
            gumballMachine.State = gumballMachine.NoQuarter;
            Console.WriteLine(MachineMessages.EjectSuccessfully);
        }

        public void Turn()
        {
            Console.WriteLine(MachineMessages.TurnSuccessfully);
            gumballMachine.State = gumballMachine.Sold;
            gumballMachine.Dispense();
        }

        public void Dispense()
        {
            Console.WriteLine(MachineMessages.DispenseWhenHasQuarter);
        }
    }
}