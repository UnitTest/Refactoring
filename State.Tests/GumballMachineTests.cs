﻿namespace State.Tests
{
    using System;
    using System.IO;
    using State;
    using NUnit.Framework;
    using FluentAssertions;

    [TestFixture]
    public class GumballMachineTests
    {
        private const int NO_GUMBALLS = 0;

        private StringWriter sw;

        [SetUp]
        public void Setup()
        {
            this.sw = new StringWriter();
            Console.SetOut(this.sw);
        }

        [Test]
        public void InsertQuarter()
        {
            var gumballMachine = CreateGumballMachine(1);

            gumballMachine.InsertQuarter();

            this.AssertMachineMessage(MachineMessages.InsertSuccessfully);
        }

        [Test]
        public void InsertQuarterTwice()
        {
            GumballMachine gumballMachine = CreateGumballMachine(1);

            gumballMachine.InsertQuarter();
            gumballMachine.InsertQuarter();

            this.AssertMachineMessage(MachineMessages.InsertTwice);
        }

        [Test]
        public void InsertWhenSoldOut()
        {
            GumballMachine gumballMachine = CreateGumballMachine(NO_GUMBALLS);

            gumballMachine.InsertQuarter();

            this.AssertMachineMessage(MachineMessages.InsertWhenSoldOut);
        }

        [Test]
        public void EjectQuarter()
        {
            GumballMachine gumballMachine = CreateGumballMachine(1);

            gumballMachine.InsertQuarter();
            gumballMachine.EjectQuarter();

            this.AssertMachineMessage(MachineMessages.EjectSuccessfully);
        }

        [Test]
        public void EjectWhenNoQuarter()
        {
            GumballMachine gumballMachine = CreateGumballMachine(1);

            gumballMachine.EjectQuarter();

            this.AssertMachineMessage(MachineMessages.EjectWhenNoQuarter);
        }

        [Test]
        public void EjectWhenSoldOut()
        {
            GumballMachine gumballMachine = CreateGumballMachine(NO_GUMBALLS);

            gumballMachine.EjectQuarter();

            this.AssertMachineMessage(MachineMessages.EjectWhenSoldOut);
        }

        [Test]
        public void TurnWhenSoldOut()
        {
            GumballMachine gumballMachine = CreateGumballMachine(NO_GUMBALLS);

            gumballMachine.TurnCrank();

            this.AssertMachineMessage(MachineMessages.TurnWhenSoldOut);
        }

        [Test]
        public void TurnWhenNoQuarter()
        {
            GumballMachine gumballMachine = CreateGumballMachine(1);

            gumballMachine.TurnCrank();

            this.AssertMachineMessage(MachineMessages.TurnWhenNoQuarter);
        }

        [Test]
        public void Dispense()
        {
            GumballMachine gumballMachine = CreateGumballMachine(1);

            gumballMachine.InsertQuarter();
            gumballMachine.TurnCrank();

            this.AssertMachineMessage(MachineMessages.TurnSuccessfully);
            this.AssertMachineMessage(MachineMessages.DispenseSuccessfully);
        }

        [Test]
        public void DispenseTheLastGumball()
        {
            GumballMachine gumballMachine = CreateGumballMachine(1);

            gumballMachine.InsertQuarter();
            gumballMachine.TurnCrank();
            gumballMachine.InsertQuarter();
            gumballMachine.TurnCrank();

            this.AssertMachineMessage(MachineMessages.DispenseTheLastGumball);
        }

        [Test]
        public void DispenseWhenNoQuarter()
        {
            GumballMachine gumballMachine = CreateGumballMachine(1);

            gumballMachine.Dispense();

            this.AssertMachineMessage(MachineMessages.DispenseWhenNoQuarter);
        }

        [Test]
        public void DispenseWhenHasQuarter()
        {
            GumballMachine gumballMachine = CreateGumballMachine(1);

            gumballMachine.InsertQuarter();
            gumballMachine.Dispense();

            this.AssertMachineMessage(MachineMessages.DispenseWhenHasQuarter);
        }

        [Test]
        public void DispenseWhenSoldOut()
        {
            GumballMachine gumballMachine = CreateGumballMachine(NO_GUMBALLS);

            gumballMachine.Dispense();

            this.AssertMachineMessage(MachineMessages.DispenseWhenSoldOut);
        }

        private static GumballMachine CreateGumballMachine(int count)
        {
            GumballMachine gumballMachine = new GumballMachine(count);
            return gumballMachine;
        }

        private void AssertMachineMessage(string message)
        {
            this.sw.ToString().Contains(message).Should().BeTrue();
        }

        [TearDown]
        public void TearDown()
        {
            this.sw.Dispose();
        }
    }
}