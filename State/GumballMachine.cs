﻿namespace State
{
    public class GumballMachine
    {
        public readonly IState SoldOut;
        public readonly IState Sold;
        public readonly IState NoQuarter;
        public readonly IState HasQuarter;

        public IState State { get; set; }

        public int Count { get; set; }

        public GumballMachine(int count)
        {
            this.SoldOut = new SoldOutState();
            this.NoQuarter = new NoQuarterState(this);
            this.HasQuarter = new HasQuarterState(this);
            this.Sold = new SoldState(this);

            State = SoldOut;
            this.Count = count;
            if (count > 0)
            {
                this.State = NoQuarter;
            }
        }

        public void InsertQuarter()
        {
            State.Insert();
        }

        public void EjectQuarter()
        {
            State.Eject();
        }

        public void TurnCrank()
        {
            State.Turn();
        }

        public void Dispense()
        {
            State.Dispense();
        }
    }
}